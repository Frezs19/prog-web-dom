<?php

$a = 12;
$b = 123.45;
$c = 'valeur textuelle';
$d = "chaîne de $c";
//$e = g; //ceci est une erreur de police

$t = ['un', 'deux', 'trois'];
$u = ['one' => 'un', 'two' => 'deux', 'three' => 'trois'];

echo "var_dump(a)=";
var_dump($a);
echo "print_r(a)=";
print_r($a);
echo "\nvar_dump(b)=";
var_dump($b);
echo "print_r(b)=";
print_r($b);
echo "\nvar_dump(c)=";
var_dump($c);
echo "print_r(c)=";
print_r($c);
echo "\nvar_dump(d)=";
var_dump($d);
echo "print_r(d)=";
print_r($d);

echo "\n\nvar_dump array :";
echo "\n t=";
var_dump($t);
echo "\n u=";
var_dump($u);

echo "\n\nprint_r array :";
echo "\n t=";
print_r($t);
echo "\n u=";
print_r($u);

# Maintenant, remplacez les appels à var_dump() par print_r()

