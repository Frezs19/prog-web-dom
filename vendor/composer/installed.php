<?php return array(
    'root' => array(
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '7ad4473f7ff9f2b7e392965adadc60225c6c2a9b',
        'name' => '__root__',
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '7ad4473f7ff9f2b7e392965adadc60225c6c2a9b',
            'dev_requirement' => false,
        ),
        'dg/rss-php' => array(
            'pretty_version' => 'v1.5',
            'version' => '1.5.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../dg/rss-php',
            'aliases' => array(),
            'reference' => '18f00ab1828948a8cfe107729ca1f11c20129b47',
            'dev_requirement' => false,
        ),
    ),
);
