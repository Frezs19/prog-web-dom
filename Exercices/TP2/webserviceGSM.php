<?php
include ('../../Helpers/tp2-helpers.php');
if (isset($_GET['top'])) {
    $nb = $_GET["top"];
} else {
    $nb = 10;
}
if (isset($_GET['op'])) {
    $op = $_GET["op"];
} else {
    $op = "";
}
if (isset($_GET['lon'])) {
    $lon = $_GET["lon"];
} else {
    $lon = 5.72752;
}
if (isset($_GET['lat'])) {
    $lat = $_GET["lat"];
} else {
    $lat = 45.19102;
}

if ($nb==""){
    $nb = 10;
}
if ($lon==""){
    $lon = 5.72752;
}
if ($lat==""){
    $lat = 45.19102;
}

$c = 0;

$file = fopen('Données-20220216/DSPE_ANT_GSM_EPSG4326.csv', 'r');
$debut = fgetcsv($file, null, ','); //On ne la sauvegarde pas dans $data car la première ligne contient juste le nom des champs
$data = array();
$location = array("lon" => $lon, "lat" => $lat);

while ( ($val = fgetcsv($file, null, ';')) !==FALSE) {
    $data[$c]['operateur'] = $val[3];
    $data[$c]['techno'] = $val[4];
    $data[$c]['lon'] = $val[5];
    $data[$c]['lat'] = $val[6];
    $data[$c]['adresse'] = $val[7];
    $data[$c]['distance'] = distance(geopoint($val[5],$val[6]), $location);
    $c = $c+1;
}

$k=0;
$dataOP = array();
for ($i=0; $i<$c; $i++) {
    if ((strcmp($data[$i]['operateur'],$op))==0) {
        $dataOP[$k]['operateur'] = $data[$i]['operateur'];
        $dataOP[$k]['techno'] = $data[$i]['techno'];
        $dataOP[$k]['adresse'] = $data[$i]['adresse'];
        $dataOP[$k]['distance'] = $data[$i]['distance'];
        $k++;
    }
}

$distances = array_column($dataOP, 'distance');
array_multisort($distances, SORT_ASC, $dataOP);

?>

<table>
  <thead>
    <tr>
      <?php
      if ($op!=="") {
        echo "<th> Numéro </th>";
        echo "<th> Opérateur </th>";
        echo "<th> Technologies </th>";
        echo "<th> Adresse </th>";
        echo "<th> Distance en m </th>";
      } else {
        echo "<h1> Il faut saisir un opérateur dans le formulaire !!! </h1>";
      }
      ?>
    </tr>
  </thead>
  <tbody>
      <?php
      if ($op!=="") {
        for ($i = 0; $i < $nb; $i++) {
            $c=$i+1;
            echo "<th> $c </th>";
            echo "<td>". $dataOP[$i]['operateur'] ."</td>";
            echo "<td>". $dataOP[$i]['techno'] ."</td>";
            echo "<td>". $dataOP[$i]['adresse'] ."</td>";
            echo "<td>". $dataOP[$i]['distance'] ."</td>";
            echo "</tr>";
        }
      }
      ?>
  </tbody>
</table>
</body>
</html>