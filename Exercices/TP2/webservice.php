<?php
include ('../../Helpers/tp2-helpers.php');
if (isset($_GET['top'])) {
    $nb = $_GET["top"];
} else {
    $nb = 10;
}
if (isset($_GET['lon'])) {
    $lon = $_GET["lon"];
} else {
    $lon = 5.72752;
}
if (isset($_GET['lat'])) {
    $lat = $_GET["lat"];
} else {
    $lat = 45.19102;
}

if ($nb==""){
    $nb = 10;
}
if ($lon==""){
    $lon = 5.72752;
}
if ($lat==""){
    $lat = 45.19102;
}

$c = 1;
$file = fopen('Données-20220216/borneswifi_EPSG4326_20171004_utf8.csv', 'r');
$debut = fgetcsv($file, null, ',');
$tab2 = array();
$tab2[0]['nom'] = $debut[0];
$tab2[0]['adr'] = $debut[1];
$tab2[0]['lon'] = $debut[2];
$tab2[0]['lat'] = $debut[3];

while ( ($data = fgetcsv($file, null, ',')) !==FALSE) {
    $tab2[$c]['nom'] = $data[0];
    $tab2[$c]['adr'] = $data[1];
    $tab2[$c]['lon'] = $data[2];
    $tab2[$c]['lat'] = $data[3];
    $c = $c+1;
}

$c=0;
$location = array("lon" => $lon, "lat" => $lat);

$listDistance = array();
foreach ($tab2 as $i => $soustab2) {
    $listDistance[$c]['nom'] = $soustab2['nom'];
    $listDistance[$c]['distance'] = distance(geopoint($soustab2['lon'],$soustab2['lat']), $location);
    $listDistance[$c]['lon'] = $soustab2['lon'];
    $listDistance[$c]['lat'] = $soustab2['lat'];
    $c++;
}
$noms  = array_column($listDistance, 'nom');
$distances = array_column($listDistance, 'distance');
array_multisort($distances, SORT_ASC, $listDistance);

?>

<table>
  <thead>
    <tr>
      <?php
        echo "<th> Numéro </th>";
        echo "<th> Nom </th>";
        echo "<th> Distance en m </th>";
      ?>
    </tr>
  </thead>
  <tbody>
      <?php
        for ($i = 0; $i < $nb; $i++) {
            $c=$i+1;
          echo "<th> $c </th>";
          echo "<td>". $listDistance[$i]['nom'] ."</td>";
          echo "<td>". $listDistance[$i]['distance'] ."</td>";
          echo "</tr>";
        }
      ?>
  </tbody>
</table>
</body>
</html>