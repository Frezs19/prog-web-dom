% PW-DOM  Compte rendu de TP2

# Compte-rendu de TP2

## Participants 

* COSTE Aurélien
* DUPONT Elise

# Points d'accès wifi
## Question 1

    cat borneswifi_EPSG4326_20171004_utf8.csv | wc -l

Résultat : 69

## Question 2

Il existe 59 emplacements différents. 

    cut -d, -f2 borneswifi_EPSG4326_20171004_utf8.csv | sort | uniq | wc -l

C'est la bibliothèque Etudes qui possède le plus de points d'accès avec 5 points d'accès.

    cut -d, -f2 borneswifi_EPSG4326_20171004.csv | sort | uniq -c | sort -n -r -b | head -1

## Question 3 & 4

Avec le comptage en PHP, on obtient 69.
Ici dans le code, on le sais en regardant la taille du tableau (tab2) qui est affiché par var_dump().
Pour être sûr de bien avoir compris, j'ai essayé avec deux tableau différents dans compte.php. Tab2 est bien plus facile à lire que tab.

Tab est un tableau de 4 élements. Chaque élement de tab est un tableau de 69 éléments. Chaque élement de tab possède un nom.

    tab {
        "nom" => {string, string, ...} (69 strings de différentes tailles)
        "adr" => {string, string, ...} (69 strings de différentes tailles)
        "lon" => {string, string, ...} (69 strings de différentes tailles)
        "lat" => {string, string, ...} (69 strings de différentes tailles)
    }

Tab2 est un tableau de 69 élements. Chaque élement de tab2 est un tableau de 4 élements. Chaque sous-tableau (élement de tab2) est composé d'un string "nom", un string "adr", un string "lon" et un string "lat".

    tab2 {
        0 => {"nom" => string "adr" => string "lon" => string "lat" => string}
        1 => {"nom" => string "adr" => string "lon" => string "lat" => string}
        ...
        68 => {"nom" => string "adr" => string "lon" => string "lat" => string}
    }

Avec tab2 il est bien plus facile de lire les différents lignes du fichier car une ligne correspond à un élement de tab2.

## Question 5

Il y a 7 points d'accès à moins de 200m de notre emplacement actuel.

Le plus proche est le point d'accès de la place Grenette, il se situe à 20.1m .

## Question 6

Pas de difficultés particulière, ici j'ai pensé à ajouter une sécurité si jamais N est supérieur à la taille du tableau.

## Question 7

Ici je rencontre un problème, je ne comprends pas la fonction smartcurl qui return un string avec lequel je ne peut pas extraire le label qui contient l'adresse obtenue avec le géocodage inverse.
On ne peut pas non plus utiliser substr car la taille de la chaîne est différente à chaque appel.

## Question 8

Seul problème, je ne peut pas afficher l'adresse précise de chaque emplacement wifi puisque je n'ai pas réussi à le faire à la question7.

## Question 9

La première ligne affiche NULL car la longitude et la latitude dans le fichier pour la première ligne ne sont pas des nombres, ce qui crée une erreur dans le format geoJSON.

## Question 10

Le formulaire webservice.html va demander à webservice.php d'afficher la table des points d'accès les plus proches.

La valeur de base est les 10 points d'accès les plus proches de la place Grenette.

    lon = 5.72752 et lat = 45.19102

# Antennes GSM
## Question 1

    cat DSPE_ANT_GSM_EPSG4326.csv | wc -l

Résulat : 101 antennes référencées.

Ce jeu contient beaucoup d'informations supplémentaires par rapport au jeu wifi, comme par exemple :
* ANT_ID
* ADRES_ID
* MICROCELL
* OPERATEUR
* ANT_TECHNO
* ANT_ADRES_LIBEL
* NUM_CARTORADIO
* NUM_SUPPORT
* ANT_2G/3G/4G

Avoir plus d'informations dans le cadre d'une démarche OpenData permet au plus d'utilisateur possible de réutiliser ce jeu de données. Plus il y d'informations différentes, plus il y a de chances que l'utilisateur trouve l'information(s) qu'il recherche et donc qu'il utilise ce jeu de données puisqu'il est ouvert à tous.

## Question 2

Pour trouver les commandes j'ai repris les mêmes que pour les points d'accès wifi en modifiant quelques caractères.

    cut -d\; -f4 DSPE_ANT_GSM_EPSG4326.csv | sort | uniq | wc -l

Résultat : 5 opérateur différents

    cut -d\; -f4 DSPE_ANT_GSM_EPSG4326.csv | sort | uniq -c | sort -n -r -b | head

Résultat :
* 30 SFR (SFR)
* 26 ORANGE (ORA)
* 26 BOUYGES (BYG)
* 18 FREE (FREE)
* 1 OPERATEUR (OPERATEUR)

## Question 3 & 4

Le format kml permet d'importer des données cartographiques dans Google Earth.

C'est compliqué de valider ce type de fichier car on ne possède pas l'accès aux données qu'il y a dedans, on ne peut pas l'ouvrir. On ne sais donc pas si le format est correct.

C'est donc un fichier compliqué en terme de facilité de lecture et d'usage.

## Question 5

Il faut que le nom de l'opérateur soit saisie correctement. Si le champ est vide j'ai mis une sécurité mais si il est mal orthographié, le code ne fonctionne pas.
On peut donc écrire :
* SFR
* ORA
* BYG
* FREE