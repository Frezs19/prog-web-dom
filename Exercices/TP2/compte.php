<?php
include ('../../Helpers/tp2-helpers.php');

$c = 1;
$file = fopen('Données-20220216/borneswifi_EPSG4326_20171004_utf8.csv', 'r');
$debut = fgetcsv($file, null, ',');
$tab = array('nom' => array($debut[0]), 'adr' => array($debut[1]), 'lon' => array($debut[2]), 'lat' => array($debut[3]));

$tab2 = array();
$tab2[0]['nom'] = $debut[0];
$tab2[0]['adr'] = $debut[1];
$tab2[0]['lon'] = $debut[2];
$tab2[0]['lat'] = $debut[3];

while ( ($data = fgetcsv($file, null, ',')) !==FALSE) {
    $tab['nom'][$c] = $data[0];
    $tab['adr'][$c] = $data[1];
    $tab['lon'][$c] = $data[2];
    $tab['lat'][$c] = $data[3];

    $tab2[$c]['nom'] = $data[0];
    $tab2[$c]['adr'] = $data[1];
    $tab2[$c]['lon'] = $data[2];
    $tab2[$c]['lat'] = $data[3];
    $c = $c+1;
}

//var_dump($tab);
//var_dump($tab2);

//tab est un petit tableau avec de grands tableaux dedans
//tab2 est un grand tableau avec de petits tableaux dedans, chaque petit tableau correspond à une ligne du fichier


//Q5

$grenette = array("lon" => "5.72752", "lat" => "45.19102");
$listDistance = array();
$c = 0;
//echo "Liste des points d'accès wifi : ";
foreach ($tab2 as $i => $soustab2) {
    //echo "<p>$soustab2[nom], distance : ";
    //echo distance(geopoint($soustab2['lon'],$soustab2['lat']), $grenette);
    //echo "m</p>";
    $listDistance[$c]['nom'] = $soustab2['nom'];
    $listDistance[$c]['distance'] = distance(geopoint($soustab2['lon'],$soustab2['lat']), $grenette);
    $c++;
}

//echo "Liste des points d'accès wifi proches (moins de 200m) : ";

foreach ($listDistance as $val) {
    if ($val['distance'] < 200) {
        //echo "<p>$val[nom], distance : $val[distance]m</p>";
    }
}

//Q6

if (isset($_GET['N'])) {
    $nb = $_GET["N"];
} else {
    $nb = 10;
}

$noms  = array_column($listDistance, 'nom');
$distances = array_column($listDistance, 'distance');

array_multisort($distances, SORT_ASC, $listDistance);

if ($nb > count($listDistance)) {
    $nb = count($listDistance);
}

//echo "Liste des ".$nb." points d'accès wifi les plus proches : ";
for ($i = 0; $i < $nb; $i++) {
    //echo "<p>".$listDistance[$i]['nom'].", distance : ".$listDistance[$i]['distance']."m</p>";
}

//Q7
$c=0;
foreach ($tab2 as $i => $soustab2) {
    //echo "<p>$soustab2[nom], distance : ";
    //echo distance(geopoint($soustab2['lon'],$soustab2['lat']), $grenette);
    //echo "m</p>";
    $listDistance[$c]['nom'] = $soustab2['nom'];
    $listDistance[$c]['distance'] = distance(geopoint($soustab2['lon'],$soustab2['lat']), $grenette);
    $listDistance[$c]['lon'] = $soustab2['lon'];
    $listDistance[$c]['lat'] = $soustab2['lat'];
    $c++;
}
$noms  = array_column($listDistance, 'nom');
$distances = array_column($listDistance, 'distance');
array_multisort($distances, SORT_ASC, $listDistance);

//echo "Liste des ".$nb." points d'accès wifi les plus proches : ";
for ($i = 0; $i < $nb; $i++) {
    //$listDistance[$i]['adresse'] = smartcurl("https://api-adresse.data.gouv.fr/reverse/?lon=".$listDistance[$i]['lon']."&lat=".$listDistance[$i]['lat'], 0);
    //echo "<p>".$listDistance[$i]['nom'].", distance : ".$listDistance[$i]['distance']."m, adresse : ".$listDistance[$i]['adresse']."</p>";
}

//Q8

if (isset($_GET['top'])) {
    $nb = $_GET["top"];
} else {
    $nb = 10;
}
if (isset($_GET['lon'])) {
    $lon = $_GET["lon"];
} else {
    $lon = 5.72752;
}
if (isset($_GET['lat'])) {
    $lat = $_GET["lat"];
} else {
    $lat = 45.19102;
}

$c=0;
$location = array("lon" => $lon, "lat" => $lat);
foreach ($tab2 as $i => $soustab2) {
    $listDistance[$c]['nom'] = $soustab2['nom'];
    $listDistance[$c]['distance'] = distance(geopoint($soustab2['lon'],$soustab2['lat']), $location);
    $listDistance[$c]['lon'] = $soustab2['lon'];
    $listDistance[$c]['lat'] = $soustab2['lat'];
    $c++;
}
$noms  = array_column($listDistance, 'nom');
$distances = array_column($listDistance, 'distance');
array_multisort($distances, SORT_ASC, $listDistance);


for ($i = 0; $i < $nb; $i++) {
    $json[$i] = '{"nom":"'.$listDistance[$i]['nom'].'","distance":"'.$listDistance[$i]['distance'].'"}';
}

for ($i = 0; $i < $nb; $i++) {
    //echo "<p>".var_dump(json_decode($json[$i]))."</p>";
}

//Q9

$file = fopen('Données-20220216/borneswifi_EPSG4326_20171004_utf8.csv', 'r');

while ( ($data = fgetcsv($file, null, ',')) !==FALSE) {
    $geoJSON=
    '{
        "type": "FeatureCollection",
        "features": [
            {
                "type": "Feature",
                "proprieties": {
                    "name":"'.$data[0].'",
                    "adr":"'.$data[1].'"
                },
                "geometry": {
                    "type":"Point",
                    "coordinates": [
                        '.$data[2].', 
                        '.$data[3].'
                    ]
                }
            }
        ]
    }';
    echo "<p>".var_dump(json_decode($geoJSON))."</p>";
}

?>