<?php
include ('../../Helpers/tp3-helpers.php');

if (isset($_GET['iden'])) {
  $idFilm = $_GET["iden"];
} else { // Sécurité pour la première connexion à la page
  $idFilm = 620;
}
// Sécurité si l'utilisateur ne rempli pas le champ
if ($idFilm==""){
  $idFilm = 620;
}

$dataVF = tmdbget('movie/'.$idFilm, ['language' => 'fr']); //appel avec paramètre, ici la langue en français
$jsonVF = json_decode($dataVF, true); //transformation du string en objet JSON
$dataVE = tmdbget('movie/'.$idFilm, ['language' => 'en']); //appel avec paramètre, ici la langue en anglais
$jsonVE = json_decode($dataVE, true); //transformation du string en objet JSON
$dataVO = tmdbget('movie/'.$idFilm, ['language' => $jsonVF['original_language']]); //appel avec paramètre, ici la langue en VO
$jsonVO = json_decode($dataVO, true); //transformation du string en objet JSON

$lienVO = 'https://www.themoviedb.org/movie/'.$idFilm.'/'.$jsonVF['original_language'];
$lienVF = 'https://www.themoviedb.org/movie/'.$idFilm.'/fr';
$lienVE = 'https://www.themoviedb.org/movie/'.$idFilm.'/en';
$url = json_decode(tmdbget('configuration'), true);
$imgVO = $url['images']['secure_base_url'].'w185'.$jsonVO['poster_path'];
$imgVF = $url['images']['secure_base_url'].'w185'.$jsonVF['poster_path'];
$imgVE = $url['images']['secure_base_url'].'w185'.$jsonVE['poster_path'];

$lienVideo = tmdbget('movie/'.$idFilm.'/videos');
$jsonLienVideo = json_decode($lienVideo, true); //transformation du string en objet JSON
foreach ($jsonLienVideo['results'] as $videos) {
  if ($videos['type']=="Trailer") {
    $trailer = "https://www.youtube.com/watch?v=".$videos['key'];
  }
}

//print_r($json); print_r affiche avec moins de détail que var_dump

/* Contenu Q3
$title = $json['title'];
$original_title = $json['original_title'];
$original_language = $json['original_language'];
$tagline = $json['tagline'];
$description = $json['overview'];
$lien = 'https://www.themoviedb.org/movie/'.$idFilm;

echo "Informations du film ayant l'identifiant ".$idFilm." :\n";

echo "<p> Titre : ".$title."</p>";
echo "<p> Titre original : ".$original_title."</p>";
echo "<p> Langue originale : ".$original_language."</p>";
echo "<p> Tagline : ".$tagline."</p>";  
echo "<p> Description : ".$description."</p>";
echo "<p> Lien : <a href=".$lien.">".$lien."</a></p>";
*/
?>
<html>
<head>
<title>Informations du film ayant l'identifiant <?php $idFilm ?></title>
<meta charset="UTF-8">
</head>
<link rel="stylesheet" href="../TP1/multiplication.css">
<body>

<form method="get" action="TMDB.php">
    <label for="iden">Identifiant :</label> <input type="text" id="iden" name="iden"/> <br />
    <input type="submit" />
</form>

<table>
  <thead>
    <tr>
      <?php
        echo "<th> x </th>";
        echo "<th> VO </th>";
        echo "<th> VF </th>";
        echo "<th> VE </th>";
      ?>
    </tr>
  </thead>
  <tbody>
      <?php
      echo "<tr><th> Titre </th>";
      echo "<td>".$jsonVO['title']."</td>";
      echo "<td>".$jsonVF['title']."</td>";
      echo "<td>".$jsonVE['title']."</td></tr>";
      echo "<tr><th> Titre Original </th>";
      echo "<td>".$jsonVO['original_title']."</td>";
      echo "<td>".$jsonVF['original_title']."</td>";
      echo "<td>".$jsonVE['original_title']."</td></tr>";
      echo "<tr><th> Original Language </th>";
      echo "<td>".$jsonVO['original_language']."</td>";
      echo "<td>".$jsonVF['original_language']."</td>";
      echo "<td>".$jsonVE['original_language']."</td></tr>";
      echo "<tr><th> Tagline </th>";
      echo "<td>".$jsonVO['tagline']."</td>";
      echo "<td>".$jsonVF['tagline']."</td>";
      echo "<td>".$jsonVE['tagline']."</td></tr>";
      echo "<tr><th> Description </th>";
      echo "<td>".$jsonVO['overview']."</td>";
      echo "<td>".$jsonVF['overview']."</td>";
      echo "<td>".$jsonVE['overview']."</td></tr>";
      echo "<tr><th> Lien </th>";
      echo "<td><a href=".$lienVO.">".$lienVO."</a></td>";
      echo "<td><a href=".$lienVF.">".$lienVF."</a></td>";
      echo "<td><a href=".$lienVE.">".$lienVE."</a></td></tr>";
      echo "<tr><th> Image </th>";
      echo "<td><img src=".$imgVO."></td>";
      echo "<td><img src=".$imgVF."></td>";
      echo "<td><img src=".$imgVE."></td></tr>";
      echo "<tr><th> Bande annonce </th>";
      echo "<td> <video width=320 height=240 controls autoplay> <source src=$trailer type=video/mp4>
            Sorry, your browser doesn't support the video element. </video> </td>";
      echo "<td> <a href=".$trailer."</a>".$trailer."<td></tr>";
      ?>
  </tbody>
</table>
</body>
</html>