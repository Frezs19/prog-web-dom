<?php
include ('../../Helpers/tp3-helpers.php');

if (isset($_GET['query'])) {
    $query = $_GET["query"];
  } else { // Sécurité pour la première connexion à la page
    $query = "Le seigneur des anneaux";
  }
  // Sécurité si l'utilisateur ne rempli pas le champ
  if ($query==""){
    $query = "Le seigneur des anneaux";
  }

$resultatsrecherche = tmdbget('search/movie', ['query' => $query]);
$resultatsrechercheFR = tmdbget('search/movie', ['query' => $query, 'language' => 'fr']); //pareil mais pour avoir le titre en français
$jsonresultatsrecherche = json_decode($resultatsrecherche, true); //transformation du string en objet JSON
$jsonresultatsrechercheFR = json_decode($resultatsrechercheFR, true); //transformation du string en objet JSON

//Raison du if expliqué dans le compte-rendu
if ($jsonresultatsrecherche['total_results'] > 20) {
    $nb = 20;
} else {
    $nb = $jsonresultatsrecherche['total_results'];
}


?>
<html>
<head>
<title>Recherche un film </title>
<meta charset="UTF-8">
</head>
<link rel="stylesheet" href="../TP1/multiplication.css">
<body>

<form method="get" action="Q6&7&9TMDB.php">
    <label for="query">Rechercher :</label> <input type="text" id="query" name="query"/> <br />
    <input type="submit" />
</form>
<h2> Résultats de recherche : <?php echo $query ?></h2>
<table>
    <thead>
        <th>Title original</th>
        <th>Titre français</th>
        <th>ID</th>
        <th>Release date</th>
        <th>Actor(s)</th>
    <tbody><?php
        for ($i=1; $i<$nb; $i++) { //Pour chaque résultat de la recherche
            echo "<tr><td>".$jsonresultatsrecherche['results'][$i]['original_title']."</td>";
            echo "<td>".$jsonresultatsrechercheFR['results'][$i]['title']."</td>";
            echo "<td>".$jsonresultatsrecherche['results'][$i]['id']."</td>";
            echo "<td>".$jsonresultatsrecherche['results'][$i]['release_date']."</td>";

            $castingquery = tmdbget('movie/'.$jsonresultatsrecherche['results'][$i]['id'].'/credits');
            $casting = json_decode($castingquery, true); //transformation du string en objet JSON
            foreach ($casting['cast'] as $person) {
                if ($person['known_for_department']=="Acting") {
                    echo "<td> <a href=Q9TMDB.php?id=".$person['id'].">".$person['original_name']."</a>\nRole: ".$person['character']."</td>";
                }
            }
        }?>
    </tbody>
</table>
</body>
</html>
