<?php
include ('../../Helpers/tp3-helpers.php');

if (isset($_GET['id'])) {
    $idActor = $_GET["id"];
  } else { // Sécurité pour la première connexion à la page
    $idActor = "1136406";
  }
  // Sécurité si l'utilisateur ne rempli pas le champ
  if ($idActor==""){
    $idActor = "1136406";
  }

$actorInformations = tmdbget('person/'.$idActor.'/movie_credits');
$jsonActorInformations = json_decode($actorInformations, true); //transformation du string en objet JSON

$actorName = tmdbget('person/'.$idActor);
$jsonActorName = json_decode($actorName, true); //transformation du string en objet JSON

?>
<html>
<head>
<title>Information acteur</title>
<meta charset="UTF-8">
</head>
<link rel="stylesheet" href="../TP1/multiplication.css">
<body>

<h2> Informations sur l'acteur : <?php echo $jsonActorName['name'] ?></h2>
<table>
    <thead>
        <th>Title original du film</th>
        <th>Rôle dans le film</th>
    <tbody><?php
        foreach ($jsonActorInformations['cast'] as $film) {
            echo "<tr><td>".$film['original_title']."</td>";
            echo "<td>".$film['character']."</td></tr>";
        }?>
    </tbody>
</table>
</body>
</html>