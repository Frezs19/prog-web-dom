% PW-DOM  Compte rendu de TP

# Compte-rendu de TP

Sujet choisi : 

## Participants 

* COSTE Aurélien
* DUPONT Elise

# Utilisation de The Movie Database
## Mise en jambes
### Question 1

Le format réponse est le JSON. Le film est Fight Club. On peut le savoir avec plusieurs champs, par exemple original_title.

Avec le paramètre :

    http://api.themoviedb.org/3/movie/550?api_key=ebb02613ce5a2ae58fde00f4db95a9c1&language=fr

On obtient les mêmes informations mais en français, logique :)

### Question 2

Avec la commande curl et le lien précédent on obtient le résultat suivant :

    {"adult":false,"backdrop_path":"/rr7E0NoGKxvbkb89eR1GwfoYjpA.jpg","belongs_to_collection":null,"budget":63000000,"genres":[{"id":18,"name":"Drama"}],"homepage":"http://www.foxmovies.com/movies/fight-club","id":550,"imdb_id":"tt0137523","original_language":"en","original_title":"Fight Club","overview":"A ticking-time-bomb insomniac and a slippery soap salesman channel primal male aggression into a shocking new form of therapy. Their concept catches on, with underground \"fight clubs\" forming in every town, until an eccentric gets in the way and ignites an out-of-control spiral toward oblivion.","popularity":71.082,"poster_path":"/pB8BM7pdSp6B6Ih7QZ4DrQ3PmJK.jpg","production_companies":[{"id":508,"logo_path":"/7PzJdsLGlR7oW4J0J5Xcd0pHGRg.png","name":"Regency Enterprises","origin_country":"US"},{"id":711,"logo_path":"/tEiIH5QesdheJmDAqQwvtN60727.png","name":"Fox 2000 Pictures","origin_country":"US"},{"id":20555,"logo_path":"/hD8yEGUBlHOcfHYbujp71vD8gZp.png","name":"Taurus Film","origin_country":"DE"},{"id":54051,"logo_path":null,"name":"Atman Entertainment","origin_country":""},{"id":54052,"logo_path":null,"name":"Knickerbocker Films","origin_country":"US"},{"id":4700,"logo_path":"/A32wmjrs9Psf4zw0uaixF0GXfxq.png","name":"The Linson Company","origin_country":"US"}],"production_countries":[{"iso_3166_1":"DE","name":"Germany"},{"iso_3166_1":"US","name":"United States of America"}],"release_date":"1999-10-15","revenue":100853753,"runtime":139,"spoken_languages":[{"english_name":"English","iso_639_1":"en","name":"English"}],"status":"Released","tagline":"Mischief. Mayhem. Soap.","title":"Fight Club","video":false,"vote_average":8.4,"vote_count":23674}

Voir Q2.php pour le programme php minimal.

### Question 3

Fichiers Q3TMDB.html et Q3TMDB.php.

Utlise un formulaire pour récupérer l'indentifiant du film et json_decode pour ensuite afficher les résultats.

## Les Choses sérieuses
### Question 4

Fichier TMDB.php (anciennement Q3TMDB.php).
Pour le CSS nous avons récupérer celui de la multiplication du TP1 pour gagner du temps car le CSS est un peu moins intéressant.

### Question 5

Fichier TMDB.php

### Question 6

Fichier Q6TMDB.php

Dans l'API il y a un champ "total_result", il indique le nombre de résultats sur les différentes pages. Or lors d'un appel tel que par exemple

    https://api.themoviedb.org/3/search/movie?api_key=ebb02613ce5a2ae58fde00f4db95a9c1&query=GhostBusters

On obtient les résultats de la première page uniquement, il y a alors une erreur si on boucle sur le nombre indiqué par "total_result" et non sur le nombre d'éléments maximum dans le json qui est équivalent au nombre d'éléments affiché au maximum sur une page, 20.

A savoir que saisir "Le seigneur des anneaux" permet d'avoir le titre des trois films + un bonus

### Question 7

Fichier Q6&Q7TMDB.php

Un appel pour obtenir le casting du film ressemble à :

    http://api.themoviedb.org/3/movie/IDFILM/credits?api_key=ebb02613ce5a2ae58fde00f4db95a9c1

Dans notre implémentation il n'y a pas le nombre de films où chaque acteur apparaît car nous avons décidés d'afficher pour chaque film les acteurs ainsi que leur rôle.
Pour savoir si c'est un acteur ou pas, nous avons décidés qu'un acteur possède le champ 'known_for_department' == "Acting"

### Question 8

Fichier Q8TMDB.php

Pour afficher tous les acteurs qui jouent des Hobbits quel que soit le film, il faudrait un champ dans l'api qui indique en plus du nom du personnage joué par l'acteur sa "race".

Puisque ce n'est pas le cas il y a un autre moyen, on peut indiquer le nom de TOUS les hobbits et faire le tri parmi les acteurs en comparant les noms des hobbits et le personnage joué par l'acteur.
Cependant cette méthode possède plusieurs défauts, il faut alimenter l'array avec le nom de hobbits mais surtout il faut que les noms de l'array correspondent avec ceux de l'API. Ce n'est pas toujours le cas lorsque par exemple dans le film "Le Seigneur des anneaux" id:123, chaque rôle est de la forme "Nom (voice)", donc on n'affiche pas les acteurs des hobbits pour ce film à cause de (voice).

### Question 9

Fichier Q6&7&9TMDB.php est la page "d'accueil" avec les liens.

Fichier Q9TMDB.php est la page d'affichage des informations de l'acteur.

Pour afficher la liste des films auxquels a participé l'acteur ainsi que la liste de ses rôles, ça serait vraiment pratique que l'API nous fournissent une liste des films dans lesquels tel acteur à joué.
En se baladant dans la documentation de l'API, nous avons trouvé notre bonheur : https://developers.themoviedb.org/3/people/get-person-movie-credits

On va alors récupérer l'id de l'acteur, puis utiliser la requête juste au dessus puis on va pouvoir afficher ses films et ses rôles.

### Question 10

Fichier TMDB.php

L'ajout de la bande annonce du film si elle est disponible n'est pas très compliquée. En revanche insérer un lecteur vidéo est un peu plus difficile.
C'est pour cela que nous avons laissé le lien à côté du lecteur.

# Analyse d'un flux RSS de podcast
## Mise en jambe
### Question 1