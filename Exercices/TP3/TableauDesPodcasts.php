<?php
header('Content-Type: text/html; charset=utf-8');

if (!ini_get('date.timezone')) {
	date_default_timezone_set('Europe/Prague');
}

require_once('../../vendor/dg/rss-php/src/Feed.php');

$rss = Feed::loadRss('http://radiofrance-podcast.net/podcast09/rss_14312.xml');

?>
<html>
<head>
<title>Tableau des podcasts</title>
<meta charset="UTF-8">
</head>
<link rel="stylesheet" href="../TP1/multiplication.css">
<body>

<form method="get" action="TableauDesPodcasts.php">
    <label for="query">Rechercher :</label> <input type="text" id="query" name="query"/> <br />
    <input type="submit" />
</form>
<h2> Le tableau des podcasts </h2>
<table>
    <thead>
        <th>Date</th>
        <th>Titre</th>
        <th>Lecture</th>
        <th>Durée</th>
        <th>Lien de téléchargement</th>
    <tbody><?php
        foreach ($rss->item as $ep) {
            echo "<tr><td>".$ep->pubDate."</td>";
            echo "<td>".$ep->title."</td>";
            echo "<td><audio controls=controls><source src=".$ep->{'enclosure'}['url']."/></audio></td>";
            echo "<td>".$ep->{'itunes:duration'}."</td>";
            echo "<td> <a href=".$ep->link.">".$ep->link."</a></td>";
        }
        ?>
    </tbody>
</table>
</body>
</html>