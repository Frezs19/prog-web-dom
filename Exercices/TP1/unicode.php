<?php
  if (isset($_GET['m1'])) {
    $m1 = $_GET["m1"];
  } else {
    $m1 = " ";
  }

  if ($m1==""){
    $m1= " ";
  }
?>


<html>
<head>
<title>Unicode</title>
<meta charset="UTF-8">
</head>
<link rel="stylesheet" href="unicode.css">
<body>

<h1>Veuillez saisir le mot</h1>

<form method="get" action="unicode.php">
    <label for="m1">Mot: </label> <input type="text" id="m1" name="m1"/> <br />
    <input type="submit" />
</form>

<p>
    <?php
        $letter = mb_substr($m1, 0, 1); //première lettre
        $unicode = mb_ord($letter, "UTF-8"); //code de la lettre
        $hexa = dechex($unicode); //code verison hexa
        $firstCar = $unicode - ($unicode%16); //premier numéro de la ligne
    ?>
</p>
<table>
  <tbody>
      <?php
        echo "<tr>";
        for ($i = 0; $i <= 16; $i++) {
          if ($firstCar==$unicode) {
            echo "<th class=surligne>";
          } else {
            echo "<th>";
          }
          printf("%c", $firstCar);
          echo  "<a href="."https://util.unicode.org/UnicodeJsps/character.jsp?a=00".dechex($firstCar).">"."U=00".dechex($firstCar)."</a>";
          $firstCar++;
          echo "</th>";
        }
        echo "</tr>";
      ?>
  </tbody>
</table>
</body>
</html>

