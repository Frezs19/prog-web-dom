<?php
    date_default_timezone_set('UTC');
    $nomJour = date("l");
    if ( ($nomJour=="January") || ($nomJour=="March") || ($nomJour=="May") || ($nomJour=="July") || ($nomJour=="August") || ($nomJour=="October") || ($nomJour=="December") ) {
        $nbJour = 31;
    } else if ( ($nomJour=="April") || ($nomJour=="June") || ($nomJour=="September") || ($nomJour=="November") ) {
        $nbJour = 30;
    } else {
        $nbJour = 28;
    }

    if (isset($_GET['mois'])) {
        $mois = $_GET["mois"];
      } else {
        $mois = "";
    }
      if (isset($_GET['an'])) {
        $an = $_GET["an"];
      } else {
        $an = "";
      }
    
      if ($mois==""){
        $mois = 3;
      }
      if ($an==""){
        $an = 2002;
      }
    
    if ($mois > 12) {
        $mois = 3;
    }
    
    if ($mois==1) {
        $nomMois = "January";
    } else if ($mois==2) {
        $nomMois = "Febuary";
    } else if ($mois==3) {
        $nomMois = "March";
    } else if ($mois==4) {
        $nomMois = "April";
    } else if ($mois==5) {
        $nomMois = "May";
    } else if ($mois==6) {
        $nomMois = "June";
    } else if ($mois==7) {
        $nomMois = "July";
    } else if ($mois==8) {
        $nomMois = "August";
    } else if ($mois==9) {
        $nomMois = "September";
    } else if ($mois==10) {
        $nomMois = "October";
    } else if ($mois==11) {
        $nomMois = "November";
    } else if ($mois==12) {
        $nomMois = "December";
    } else {
        $nomMois = "March";
    }
?>

<html>
<head>
<title>Calendrier</title>
<meta charset="UTF-8">
</head>
<link rel="stylesheet" href="calendrier.css">
<body>

<form method="get" action="calendrier.php">
    <label for="mois">Mois (à saisir en numéro)</label> <input type="text" id="mois" name="mois"/> <br />
    <label for="an">Année</label> <input type="text" id="an" name="an"/> <br />
    <input type="submit" />
</form>


<h1><?php echo $nomMois ?></h1>
<table>
    <?php
        for ($i=1; $i<=$nbJour; $i++) {
            echo "<tr> <td>".date("l", mktime(0, 0, 0, $mois, $i, $an))."</td> <td> $i </td> <td> </td> </tr>";
        }
    ?>
</table>

</body>
</html>

