<?php
  if (isset($_GET['ligne'])) {
    $ligne = $_GET["ligne"];
  } else {
    $ligne = 0;
  }
  if (isset($_GET['colonne'])) {
    $col = $_GET["colonne"];
  } else {
    $col = 0;
  }
  if (isset($_GET['choix'])) {
    $choix = $_GET["choix"];
  } else {
    $choix = 0;
  }

  if ($ligne==""){
    $ligne = 10;
  }
  if ($col==""){
    $col = 10;
  }
  if ($col==""){
    $choix = 0;
  }
?>


<html>
<head>
<title>Tables de multiplication</title>
<meta charset="UTF-8">
</head>
<link rel="stylesheet" href="multiplication.css">
<body>

<h1>Voici une belle table de multiplication</h1>

<form method="get" action="multiplication.php">
    <label for="ligne">Ligne</label> <input type="text" id="ligne" name="ligne"/> <br />
    <label for="col">Colonne</label> <input type="text" id="colonne" name="colonne"/> <br />
    <label for="choix">Choix surlignée</label> <input type="text" id="choix" name="choix"/> <br />
    <input type="submit" />
</form>

<table>
  <thead>
    <tr>
      <?php
        echo "<th> x </th>";
        for ($i = 0; $i <= $ligne; $i++) {
          echo "<th> $i </th>";
        }
      ?>
    </tr>
  </thead>
  <tbody>
      <?php
        for ($i = 0; $i <= $col; $i++) {
          if ($i==$choix) {
            echo "<tr class=surligne>";
          } else {
            echo "<tr>";
          }
          echo "<th> $i </th>";
          for ($j = 0; $j <= $ligne; $j++) {
            $res = $i*$j;
            echo "<td> $res </td>";
          }
          echo "</tr>";
        }
      ?>
  </tbody>
</table>
</body>
</html>

