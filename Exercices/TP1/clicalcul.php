<?php

require_once 'libcalcul.php';

if ($argc > 3) {
    $somme = $argv[1];
    $taux = $argv[2];
    $duree = $argv[3];
} else {
    echo "Il manque des arguments à la fonction, valeurs de base utilisées\n";
    $somme = 0;
    $taux = 1;
    $duree = 1;
}

calcul($somme, $taux, $duree);
